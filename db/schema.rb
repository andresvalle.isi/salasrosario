# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130727001111) do

  create_table "bands", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "cancellations", :force => true do |t|
    t.integer  "reservation_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "musicians", :force => true do |t|
    t.integer  "dni"
    t.string   "name"
    t.string   "lastname"
    t.string   "phone"
    t.string   "mobile"
    t.binary   "debtor"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reservation_types", :force => true do |t|
    t.integer  "code"
    t.string   "denomination"
    t.string   "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "reservations", :force => true do |t|
    t.integer  "reservation_type_id"
    t.integer  "room_id"
    t.integer  "user_id"
    t.integer  "musician_id"
    t.integer  "band_id"
    t.date     "virtual_day"
    t.time     "since"
    t.time     "until"
    t.decimal  "amount_paid"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "room_prices", :force => true do |t|
    t.integer  "room_id"
    t.date     "since"
    t.decimal  "amount"
    t.integer  "reservation_type_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.binary   "available"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.integer  "dni"
    t.string   "login"
    t.string   "passwd"
    t.string   "name"
    t.string   "lastname"
    t.string   "role"
    t.binary   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
