# Examples:
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

reservation_types = ReservationType.create([	
	{ code: 1, denomination: 'Fijo', description: 'Reserva de tipo fijo' },
	{ code: 2, denomination: 'No Fijo', description: 'Reserva de tipo no fijo' }
])

rooms = Room.create([
	{ id: 1, name: 'SALA 1', available: 1 },
	{ id: 2, name: 'SALA 2', available: 1 },
	{ id: 3, name: 'SALA 3', available: 1 }
])

# room_prices = RoomPrice.create([
# 	{ since: Date.current, amount: '30', room: rooms[0], reservation_type: reservation_types[0] },
# 	{ since: Date.current, amount: '32', room: rooms[0], reservation_type: reservation_types[1] },
# 	{ since: Date.current, amount: '40', room: rooms[1], reservation_type: reservation_types[0] },
# 	{ since: Date.current, amount: '42', room: rooms[1], reservation_type: reservation_types[1] },
# 	{ since: Date.current, amount: '50', room: rooms[2], reservation_type: reservation_types[0] },
# 	{ since: Date.current, amount: '52', room: rooms[2], reservation_type: reservation_types[1] }
# ])

users = User.create([
	{ dni: 11111111, login: 'webmaster', passwd: 'asd', name: 'webmaster', lastname: 'webmaster', role: 'W', status: 1 },
	{ dni: 22222222, login: 'admin', passwd: 'asd', name: 'admin', lastname: 'admin', role: 'A', status: 1 },
	{ dni: 33333333, login: 'encargado', passwd: 'asd', name: 'encargado', lastname: 'encargado', role: 'E', status: 1 }
])

musicians = Musician.create([
	{ dni: 36159485, name: 'Javier', lastname: 'Valle', phone: 4811627, mobile: 158456987, debtor: 0 },
	{ dni: 34933298, name: 'Andres', lastname: 'Valle', phone: 4811627, mobile: 153317556, debtor: 0 },
	{ dni: 34123456, name: 'Matias', lastname: 'Duarte', phone: 4823770, mobile: 150325489, debtor: 0 }
])

bands = Band.create([
	{ name: 'Led Zeppelin' },
	{ name: 'The Police' },
	{ name: 'Pink Floyd' },
	{ name: 'AC/DC' }
])