class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :reservation_type_id
      t.integer :room_id
      t.integer :user_id
      t.integer :musician_id
      t.integer :band_id
      t.date :virtual_day
      t.time :since
      t.time :until
      t.decimal :amount_paid

      t.timestamps
    end
  end
end
