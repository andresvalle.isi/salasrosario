class CreateReservationTypes < ActiveRecord::Migration
  def change
    create_table :reservation_types do |t|
      t.integer :code
      t.string :denomination
      t.string :description

      t.timestamps
    end
  end
end
