class CreateCancellations < ActiveRecord::Migration
  def change
    create_table :cancellations do |t|
      t.integer :reservation_id
      t.integer :user_id

      t.timestamps
    end
  end
end
