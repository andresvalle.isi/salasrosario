class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :dni
      t.string :login  
      t.string :passwd
      t.string :name
      t.string :lastname
      t.string :role
      t.binary :status

      t.timestamps
    end
  end
end
