class CreateRoomPrices < ActiveRecord::Migration
  def change
    create_table :room_prices do |t|
      t.integer :room_id
      t.date :since
      t.decimal :amount
      t.integer :reservation_type_id

      t.timestamps
    end
  end
end
