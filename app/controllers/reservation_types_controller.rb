class ReservationTypesController < ApplicationController
  # GET /reservation_types
  # GET /reservation_types.json
  def index
    @reservation_types = ReservationType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reservation_types }
    end
  end

  # GET /reservation_types/1
  # GET /reservation_types/1.json
  def show
    @reservation_type = ReservationType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reservation_type }
    end
  end

  # GET /reservation_types/new
  # GET /reservation_types/new.json
  def new
    @reservation_type = ReservationType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reservation_type }
    end
  end

  # GET /reservation_types/1/edit
  def edit
    @reservation_type = ReservationType.find(params[:id])
  end

  # POST /reservation_types
  # POST /reservation_types.json
  def create
    @reservation_type = ReservationType.new(params[:reservation_type])

    respond_to do |format|
      if @reservation_type.save
        format.html { redirect_to @reservation_type, notice: 'Reservation type was successfully created.' }
        format.json { render json: @reservation_type, status: :created, location: @reservation_type }
      else
        format.html { render action: "new" }
        format.json { render json: @reservation_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reservation_types/1
  # PUT /reservation_types/1.json
  def update
    @reservation_type = ReservationType.find(params[:id])

    respond_to do |format|
      if @reservation_type.update_attributes(params[:reservation_type])
        format.html { redirect_to edit_reservation_type_path(@reservation_type), notice: 'Reservation type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reservation_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservation_types/1
  # DELETE /reservation_types/1.json
  def destroy
    @reservation_type = ReservationType.find(params[:id])
    @reservation_type.destroy

    respond_to do |format|
      format.html { redirect_to reservation_types_url }
      format.json { head :no_content }
    end
  end
end
