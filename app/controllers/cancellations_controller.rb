class CancellationsController < ApplicationController
  # GET /cancellations
  # GET /cancellations.json
  def index
    @cancellations = Cancellation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cancellations }
    end
  end

  # GET /cancellations/1
  # GET /cancellations/1.json
  def show
    @cancellation = Cancellation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cancellation }
    end
  end

  # GET /cancellations/new
  # GET /cancellations/new.json
  def new
    @cancellation = Cancellation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cancellation }
    end
  end

  # GET /cancellations/1/edit
  def edit
    @cancellation = Cancellation.find(params[:id])
  end

  # POST /cancellations
  # POST /cancellations.json
  def create
    @cancellation = Cancellation.new(params[:cancellation])

    respond_to do |format|
      if @cancellation.save
        format.html { redirect_to @cancellation, notice: 'Cancellation was successfully created.' }
        format.json { render json: @cancellation, status: :created, location: @cancellation }
      else
        format.html { render action: "new" }
        format.json { render json: @cancellation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cancellations/1
  # PUT /cancellations/1.json
  def update
    @cancellation = Cancellation.find(params[:id])

    respond_to do |format|
      if @cancellation.update_attributes(params[:cancellation])
        format.html { redirect_to @cancellation, notice: 'Cancellation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cancellation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cancellations/1
  # DELETE /cancellations/1.json
  def destroy
    @cancellation = Cancellation.find(params[:id])
    @cancellation.destroy

    respond_to do |format|
      format.html { redirect_to cancellations_url }
      format.json { head :no_content }
    end
  end
end
