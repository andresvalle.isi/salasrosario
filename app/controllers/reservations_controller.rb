class ReservationsController < ApplicationController
  # GET /reservations
  # GET /reservations.json
  def index
    virtual_day = params[:virtual_day]
    @reservations = Reservation.select("id, room_id, musician_id, band_id, virtual_day, since, until")
                               # .where("since = ?", virtual_day || Date.current)
# AND cancelled IS NOT NULL
    
    @rooms = Room.all
    @musicians = Musician.all
    @bands = Band.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reservations }
    end
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
    @reservation = Reservation.find(params[:id])
    @musician = @reservation.musician
    @band = @reservation.band
    @room = @reservation.room

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reservation }
    end
  end

  # GET /reservations/new
  # GET /reservations/new.json
  def new
    @reservation = Reservation.new
    @rooms = Room.all
    @musicians = Musician.all
    @bands = Band.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reservation }
    end
  end

  # GET /reservations/1/edit
  def edit
    @reservation = Reservation.find(params[:id])
    @rooms = Room.all
    @musicians = Musician.all
    @bands = Band.all
  end

  # POST /reservations
  # POST /reservations.json
  def create
    params[:reservation][:virtual_day] = Date.current
    @reservation = Reservation.new(params[:reservation])

    respond_to do |format|
      if @reservation.save
        format.html { redirect_to reservations_url, notice: 'Reservation was successfully created.' }
        format.json { render json: @reservation, status: :created, location: @reservation }
      else
        format.html { render action: "new" }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reservations/1
  # PUT /reservations/1.json
  def update
    @reservation = Reservation.find(params[:id])

    respond_to do |format|
      if @reservation.update_attributes(params[:reservation])
        format.html { redirect_to edit_reservation_path(@reservation), notice: 'Reservation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation = Reservation.find(params[:id])
    @reservation.destroy

    respond_to do |format|
      format.html { redirect_to reservations_url }
      format.json { head :no_content }
    end
  end
end