class RoomPricesController < ApplicationController
  # GET /room_prices
  # GET /room_prices.json
  def index
    # mostrar los precios actuales. ultimo precio cargado por sala, con fecha menor o igual a hoy.
    @room_prices = RoomPrice.select("id, room_id, reservation_type_id, since, amount")
                            .where("since <= ?", Date.current)
                            .group("room_id, reservation_type_id")
                            .having("MAX(date(created_at))")
                            .order("room_id, reservation_type_id")


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @room_prices }
    end
  end

  # GET /room_prices/1
  # GET /room_prices/1.json
  def show
    room_price = RoomPrice.find(params[:id])
    @room = room_price.room
    @reservation_type = room_price.reservation_type
    @room_prices = RoomPrice.select("*").where("room_id = :room_id AND reservation_type_id = :reservation_type_id",
      { room_id: @room.id, reservation_type_id: @reservation_type.id }).order("since desc")

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @room_price }
    end
  end

  # GET /room_prices/new
  # GET /room_prices/new.json
  def new
    @room_price = RoomPrice.new
    @rooms = Room.all
    @reservation_types = ReservationType.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @room_price }
    end
  end

  # GET /room_prices/1/edit
  def edit
    @room_price = RoomPrice.find(params[:id])
    @rooms = Room.all
    @reservation_types = ReservationType.all
  end

  # POST /room_prices
  # POST /room_prices.json
  def create
    @room_price = RoomPrice.new(params[:room_price])
    @rooms = Room.all
    @reservation_types = ReservationType.all

    respond_to do |format|
      if @room_price.save
        format.html { redirect_to room_prices_url, notice: 'Room price was successfully created.' }
        format.json { render json: @room_price, status: :created, location: @room_price }
      else
        format.html { render action: "new" }
        format.json { render json: @room_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /room_prices/1
  # PUT /room_prices/1.json
  def update
    @room_price = RoomPrice.find(params[:id])

    respond_to do |format|
      if @room_price.update_attributes(params[:room_price])
        format.html { redirect_to edit_room_price_path(@room_price), notice: 'Room price was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @room_price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /room_prices/1
  # DELETE /room_prices/1.json
  def destroy
    @room_price = RoomPrice.find(params[:id])
    @room_price.destroy

    respond_to do |format|
      format.html { redirect_to room_prices_url, notice: 'Room price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
end
