class MusiciansController < ApplicationController
  # GET /musicians
  # GET /musicians.json
  def index
    @musicians = Musician.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @musicians }
    end
  end

  # GET /musicians/1
  # GET /musicians/1.json
  def show
    @musician = Musician.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @musician }
    end
  end

  # GET /musicians/new
  # GET /musicians/new.json
  def new
    @musician = Musician.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @musician }
    end
  end

  # GET /musicians/1/edit
  def edit
    @musician = Musician.find(params[:id])
  end

  # POST /musicians
  # POST /musicians.json
  def create
    musician_params = params[:musician]
    musician_params[:debtor] = 0
    @musician = Musician.new(musician_params)

    respond_to do |format|
      if @musician.save
        format.html { redirect_to musicians_url, notice: "Musician #{@musician.name} #{@musician.lastname} was successfully created." }
        format.json { render json: @musician, status: :created, location: @musician }
      else
        format.html { render action: "new" }
        format.json { render json: @musician.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /musicians/1
  # PUT /musicians/1.json
  def update
    @musician = Musician.find(params[:id])
    musician_params = params[:musician]
    musician_params[:debtor] = @musician.debtor

    respond_to do |format|
      if @musician.update_attributes( musician_params )
        format.html { redirect_to edit_musician_path(@musician), notice: 'Musician was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @musician.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /musicians/1
  # DELETE /musicians/1.json
  # def destroy
  #   @musician = Musician.find(params[:id])
  #   @musician.destroy

  #   respond_to do |format|
  #     format.html { redirect_to musicians_url }
  #     format.json { head :no_content }
  #   end
  # end

end
