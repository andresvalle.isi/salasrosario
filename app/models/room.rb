class Room < ActiveRecord::Base
  has_many :room_prices
  has_many :reservations
  attr_accessible :room_id, :available, :name
  validates :available, :name, :presence => true
  validates :name, :uniqueness => true
end
