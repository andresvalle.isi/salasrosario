class Reservation < ActiveRecord::Base
  # has_one :reservation_type
  belongs_to :room
  # has_one :cancellation
  # has_one :user 
  belongs_to :musician
  belongs_to :band
  attr_accessible :reservation_id, :since, :until, :virtual_day, :paid_amount, :room_id, :musician_id, :band_id
  validates :room_id, :musician_id, :since, :until, :virtual_day, :presence => true
end