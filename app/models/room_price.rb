class RoomPrice < ActiveRecord::Base
  belongs_to :room
  belongs_to :reservation_type
  attr_accessible :room, :since, :amount, :reservation, :room_id, :reservation_type_id
  validates :since, :amount, :presence => true
  validates_uniqueness_of :room_id, scope: [:reservation_type_id, :since], message: "already has a defined amount by the specified date"
  #validates_associated :rooms
end
