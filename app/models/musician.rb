class Musician < ActiveRecord::Base
  # has_and_belongs_to_many :bands
  has_many :reservations
  attr_accessible :musician_id, :dni, :name, :lastname, :phone, :mobile, :debtor
  validates :name, :lastname, :phone, :mobile, :presence => true
  validates :debtor, :inclusion => { :in => [1, 0] }
  validates :dni, :uniqueness => true
end
