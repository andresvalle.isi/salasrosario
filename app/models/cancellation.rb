class Cancellation < ActiveRecord::Base
  has_one :reservation
  has_one :user
  attr_accessible :reservation_id, :user_id
  validates :reservation, :user, :presence => true
end
