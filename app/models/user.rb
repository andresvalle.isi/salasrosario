class User < ActiveRecord::Base
  has_many :reservations
  has_many :cancellations
  attr_accessible :user_id, :dni, :login, :passwd, :lastname, :name, :role, :status
  validates :dni, :login, :passwd, :lastname, :name, :role, :status, :presence => true
  validates :dni, :login, :uniqueness => true

  def role_denomination
    case self.role
      when '3' then return 'Webmaster'
      when '2' then return 'Administrador'
      else return 'Encargado'
    end
  end

end
