class ReservationType < ActiveRecord::Base
  has_one :reservation
  has_one :room_price
  attr_accessible :reservation_type_id, :code, :denomination, :description
  validates :code, :denomination, :description, :presence => true
  validates :code, :denomination, :uniqueness => true
end
