class Band < ActiveRecord::Base
  # has_and_belongs_to_many :musicians
  has_many :reservations
  attr_accessible :band_id, :name
  validates :name, :presence => true
  validates :name, :uniqueness => true
end