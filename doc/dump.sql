CREATE DATABASE  IF NOT EXISTS `salasrosario` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `salasrosario`;
-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: salasrosario
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesorios`
--

DROP TABLE IF EXISTS `accesorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesorios` (
  `id_accesorio` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `marca` varchar(25) NOT NULL,
  `modelo` varchar(25) DEFAULT NULL,
  `estado` enum('D','N') NOT NULL,
  `id_complejo` int(11) unsigned NOT NULL,
  `precio` decimal(10,0) unsigned NOT NULL,
  PRIMARY KEY (`id_accesorio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesorios`
--

LOCK TABLES `accesorios` WRITE;
/*!40000 ALTER TABLE `accesorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alquileres`
--

DROP TABLE IF EXISTS `alquileres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alquileres` (
  `id_accesorio` int(10) unsigned NOT NULL,
  `id_reserva` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_accesorio`,`id_reserva`),
  KEY `id_reserva` (`id_reserva`),
  CONSTRAINT `alquileres_ibfk_1` FOREIGN KEY (`id_accesorio`) REFERENCES `accesorios` (`id_accesorio`),
  CONSTRAINT `alquileres_ibfk_2` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id_reserva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alquileres`
--

LOCK TABLES `alquileres` WRITE;
/*!40000 ALTER TABLE `alquileres` DISABLE KEYS */;
/*!40000 ALTER TABLE `alquileres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bandas`
--

DROP TABLE IF EXISTS `bandas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandas` (
  `id_banda` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `fecha_alta` date NOT NULL,
  `id_complejo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_banda`),
  KEY `id_complejo` (`id_complejo`),
  CONSTRAINT `bandas_ibfk_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bandas`
--

LOCK TABLES `bandas` WRITE;
/*!40000 ALTER TABLE `bandas` DISABLE KEYS */;
/*!40000 ALTER TABLE `bandas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bandas_musicos`
--

DROP TABLE IF EXISTS `bandas_musicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandas_musicos` (
  `id_musico` int(11) unsigned NOT NULL,
  `id_banda` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_musico`,`id_banda`),
  KEY `id_banda` (`id_banda`),
  CONSTRAINT `bandas_musicos_ibfk_1` FOREIGN KEY (`id_musico`) REFERENCES `musicos` (`id_musico`),
  CONSTRAINT `bandas_musicos_ibfk_2` FOREIGN KEY (`id_banda`) REFERENCES `bandas` (`id_banda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bandas_musicos`
--

LOCK TABLES `bandas_musicos` WRITE;
/*!40000 ALTER TABLE `bandas_musicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `bandas_musicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complejos`
--

DROP TABLE IF EXISTS `complejos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complejos` (
  `id_complejo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `direccion` varchar(25) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_complejo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complejos`
--

LOCK TABLES `complejos` WRITE;
/*!40000 ALTER TABLE `complejos` DISABLE KEYS */;
/*!40000 ALTER TABLE `complejos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deudas`
--

DROP TABLE IF EXISTS `deudas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deudas` (
  `id_deuda` int(10) unsigned NOT NULL,
  `id_complejo` int(10) unsigned NOT NULL,
  `id_musico` int(10) unsigned NOT NULL,
  `id_banda` int(10) unsigned NOT NULL,
  `razon` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_deuda`),
  KEY `fk_deudas_1_idx` (`id_complejo`),
  KEY `fk_deudas_2_idx` (`id_musico`),
  KEY `fk_deudas_3_idx` (`id_banda`),
  CONSTRAINT `fk_deudas_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_deudas_2` FOREIGN KEY (`id_musico`) REFERENCES `musicos` (`id_musico`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_deudas_3` FOREIGN KEY (`id_banda`) REFERENCES `bandas` (`id_banda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deudas`
--

LOCK TABLES `deudas` WRITE;
/*!40000 ALTER TABLE `deudas` DISABLE KEYS */;
/*!40000 ALTER TABLE `deudas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `id_complejo` int(11) unsigned NOT NULL,
  `dia` int(1) unsigned NOT NULL,
  `desde` time NOT NULL,
  `hasta` time NOT NULL,
  PRIMARY KEY (`id_complejo`,`dia`),
  KEY `fk_horarios_1_idx` (`id_complejo`),
  CONSTRAINT `fk_horarios_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarios`
--

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musicos`
--

DROP TABLE IF EXISTS `musicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musicos` (
  `id_musico` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `telefono_fijo` varchar(25) DEFAULT NULL,
  `telefono_celular` varchar(25) DEFAULT NULL,
  `fecha_alta` date NOT NULL,
  `id_complejo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_musico`),
  KEY `id_complejo` (`id_complejo`),
  CONSTRAINT `musicos_ibfk_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musicos`
--

LOCK TABLES `musicos` WRITE;
/*!40000 ALTER TABLE `musicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `musicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precios_accesorios`
--

DROP TABLE IF EXISTS `precios_accesorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precios_accesorios` (
  `fecha_desde` date NOT NULL,
  `monto` varchar(45) NOT NULL,
  `id_accesorio` int(11) unsigned NOT NULL,
  PRIMARY KEY (`fecha_desde`,`id_accesorio`),
  KEY `fk_precios_accesorios_1_idx` (`id_accesorio`),
  CONSTRAINT `fk_precios_accesorios_1` FOREIGN KEY (`id_accesorio`) REFERENCES `accesorios` (`id_accesorio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precios_accesorios`
--

LOCK TABLES `precios_accesorios` WRITE;
/*!40000 ALTER TABLE `precios_accesorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `precios_accesorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precios_salas`
--

DROP TABLE IF EXISTS `precios_salas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precios_salas` (
  `fecha_desde` date NOT NULL,
  `banda_fija` decimal(10,0) NOT NULL,
  `banda_no_fija` decimal(10,0) NOT NULL,
  `id_sala` int(11) unsigned NOT NULL,
  KEY `id_sala` (`id_sala`),
  CONSTRAINT `precios_salas_ibfk_1` FOREIGN KEY (`id_sala`) REFERENCES `salas` (`id_sala`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precios_salas`
--

LOCK TABLES `precios_salas` WRITE;
/*!40000 ALTER TABLE `precios_salas` DISABLE KEYS */;
/*!40000 ALTER TABLE `precios_salas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas`
--

DROP TABLE IF EXISTS `reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservas` (
  `id_reserva` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_musico` int(10) unsigned NOT NULL,
  `id_banda` int(10) unsigned DEFAULT NULL,
  `fecha_desde` datetime NOT NULL,
  `fecha_hasta` datetime NOT NULL,
  `fecha_reserva` datetime NOT NULL,
  `dia_virtual` date NOT NULL,
  `fecha_cancelacion` datetime DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `id_sala` int(10) unsigned NOT NULL,
  `tipo_reserva` enum('F','N') NOT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `fk_reservas_1_idx` (`id_sala`),
  KEY `fk_reservas_2_idx` (`id_musico`),
  KEY `fk_reservas_3_idx` (`id_banda`),
  CONSTRAINT `fk_reservas_1` FOREIGN KEY (`id_sala`) REFERENCES `salas` (`id_sala`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservas_2` FOREIGN KEY (`id_musico`) REFERENCES `musicos` (`id_musico`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservas_3` FOREIGN KEY (`id_banda`) REFERENCES `bandas` (`id_banda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas`
--

LOCK TABLES `reservas` WRITE;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salas`
--

DROP TABLE IF EXISTS `salas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salas` (
  `id_sala` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `estado` char(1) NOT NULL,
  `id_complejo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_sala`),
  KEY `id_complejo` (`id_complejo`),
  CONSTRAINT `salas_ibfk_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salas`
--

LOCK TABLES `salas` WRITE;
/*!40000 ALTER TABLE `salas` DISABLE KEYS */;
/*!40000 ALTER TABLE `salas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `id_complejo` int(11) unsigned NOT NULL,
  `fecha_alta` date NOT NULL,
  `perfil` enum('W','E','D') NOT NULL,
  `estado` char(1) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_complejo` (`id_complejo`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_complejo`) REFERENCES `complejos` (`id_complejo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-11  0:09:03
