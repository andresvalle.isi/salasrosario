* User
dni:integer
passwd:string
name:string
lastname:string
role:string
status:binary

* Room
name:string
available:binary

* RoomPrice
normal_prize:decimal
reservation_prize:decimal
since:date
room_id:integer

* Band
name:string

* Musician
passwd:string
name:string
lastname:string
phone:string
mobile:string
registration:date

* Reservation
musician_id:integer
band_id:integer
room_id:integer
since:time
to:time
virtual_day:date
reserved:time
cancelled:time

-------------------------------------

dni:integer passwd:string name:string lastname:string role:string status:binary

name:string available:binary

normal_prize:decimal reservation_prize:decimal since:date room_id:integer 

passwd:string name:string lastname:string phone:string mobile:string registration:date

musician_id:integer band_id:integer room_id:integer since:time to:time virtual_day:date reserved:time cancelled:time